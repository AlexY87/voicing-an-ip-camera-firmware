#How to run a script
#python3 iris_translation.py --filename data.json

from boto3 import Session
from botocore.exceptions import BotoCoreError, ClientError
from contextlib import closing
import os
import sys
import subprocess
import json
import argparse
from tempfile import gettempdir

def create_cmd_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename', action="store", dest="filename")
    return parser

parser = create_cmd_parser()
args = parser.parse_args()

def synthesize_text(filename, message, voice):
    session = Session(profile_name="default")
    polly = session.client("polly")
    try:
        response = polly.synthesize_speech(Text=message, OutputFormat="mp3", VoiceId=voice)
    except (BotoCoreError, ClientError) as error:
        print(error)
        sys.exit(-1)
    if "AudioStream" in response:
            with closing(response["AudioStream"]) as stream:
                output = filename+".mp3"
                try:
                    with open(output, "wb") as file:
                        file.write(stream.read())
                        print("File {} is ready".format(output))
                except IOError as error:
                    print(error)
                    sys.exit(-1)
    else:
        print("Could not stream audio")
        sys.exit(-1)

def main():
    if args.filename == None:
        print("Argument \"--filename\" is missed")
        sys.exit(-1)
    
    with open(args.filename, "r") as read_file:
        data = json.load(read_file)
    #print(data["voice"])
    for key in data["phrases"].keys():
        synthesize_text(filename=key, message=data["phrases"][key], voice=data["voice"])
        #print("File {}.{} is created".format(key,"mp3"))

if __name__ == "__main__":
    main()